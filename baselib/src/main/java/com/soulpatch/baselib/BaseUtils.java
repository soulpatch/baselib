package com.soulpatch.baselib;

import android.graphics.Paint;
import android.widget.TextView;

/**
 * Util class that holds methods and constants for the API
 *
 * @author Akshay Viswanathan
 */
public class BaseUtils {
    public static String SHARED_PREF_API_KEY;
    public static String SHARED_PREF_API_KEY_STRING;
    public static String SHARED_PREF_BASE_URL;

    /**
     * Given a text view, make the string strikethrough or remove it.
     *
     * @param textView {@link TextView} that contains the string to be manipulated
     * @param flag true if the string needs to be struck-through, false otherwise
     */
    public static void strikeThroughText(final TextView textView, final boolean flag) {
        if (flag) {
            textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            textView.setPaintFlags(textView.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        }
    }
}
