package com.soulpatch.baselib.base;

import com.soulpatch.baselib.transaction.RequestType;

/**
 * @author Akshay Viswanathan.
 */
public interface ITransactionData {
    /**
     * Get the url that needs to be hit to request this data
     */
    String getUrl();

    /**
     * Specify the {@link RequestType}
     */
    RequestType getRequestType();
}
