package com.soulpatch.baselib.base;

import com.soulpatch.baselib.transaction.RequestType;

/**
 * Common base class that all the data-models should implement
 *
 * @author Akshay Viswanathan
 */
public class BaseData implements ITransactionData {
    public String mName;

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUrl() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RequestType getRequestType() {
        return null;
    }
}