package com.soulpatch.baselib.transaction;

/**
 * Enumeration with the possible REST calls
 *
 * @author Akshay Viswanathan.
 */
public enum RequestType {
    GET("GET"),
    PUT("PUT"),
    POST("POST"),
    DELETE("DELETE");

    final String mValue;

    RequestType(final String value) {
        mValue = value;
    }

    public String getValue() {
        return mValue;
    }
}
