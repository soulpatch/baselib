package com.soulpatch.baselib.transaction;

/**
 * Callback to call into from the onPostExecute of the RestCallerTask
 *
 * @author Akshay Viswanathan
 */
public interface PostExecuteCallback {
    /**
     * Method to return the response of the async call
     *
     * @param responseObj {@link String} returned as a response
     * @param exception {@link Exception} encountered during doInBackground operation to be sent to the user to take necessary action
     */
    void onResponse(final String responseObj, final Exception exception);
}
