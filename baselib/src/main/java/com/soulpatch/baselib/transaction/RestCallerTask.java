package com.soulpatch.baselib.transaction;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.soulpatch.baselib.BaseUtils;
import com.soulpatch.baselib.base.QueryParam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * All REST-ful API calls should be made using this class
 *
 * @author Akshay Viswanathan
 */
public class RestCallerTask extends AsyncTask<QueryParam, Void, String> {
    private static final String TAG = RestCallerTask.class.getSimpleName();
    private final PostExecuteCallback mPostExecuteCallback;
    private final String mRequestURL;
    private final RequestType mRequestType;
    private Exception mException;

    /**
     * Constructor that takes all the necessary arguments
     *
     * @param context current context of the application
     * @param requestURL url that comes after the base url (like: /jersey/rest). NOTE: This is different from the params
     * @param requestType {@link RequestType} that needs to be called on the url
     * @param postExecuteCallback {@link PostExecuteCallback} call back to be called in onPostExecute
     * <p>
     * NOTE : Consumer must set the SHARED_PREF_FILE_NAME constant in the {@link BaseUtils} class so this method can read the credentials from the {@link SharedPreferences}
     */
    public RestCallerTask(@NonNull final Context context, @NonNull final String requestURL, @NonNull final RequestType requestType,
                          @NonNull final PostExecuteCallback postExecuteCallback) {
        //noinspection ConstantConditions
        if (requestType == null || postExecuteCallback == null || context == null) {
            throw new IllegalArgumentException("None of the arguments passed to the constructor can be null");
        }

        mPostExecuteCallback = postExecuteCallback;
        mRequestURL = requestURL;
        mRequestType = requestType;
    }

    /**
     * Hits the url created using the Base URL provided in the transactionData.json file and appending the requestURL to it and adding other query params. Returns the response received from the query
     * as a string. {@inheritDoc}
     */
    @Override
    protected String doInBackground(final QueryParam... params) {
        final String baseUrl = BaseUtils.SHARED_PREF_BASE_URL;
        final String apiKey = BaseUtils.SHARED_PREF_API_KEY;
        final String apiKeyString = BaseUtils.SHARED_PREF_API_KEY_STRING;

        if (TextUtils.isEmpty(baseUrl) || TextUtils.isEmpty(apiKey) || TextUtils.isEmpty(apiKeyString)) {
            throw new IllegalStateException("One of base url, api key or api key string is invalid. Please initialize the object with valid arguments");
        }

        final Uri.Builder uriBuilder = Uri.parse(baseUrl).buildUpon();

        if (!TextUtils.isEmpty(mRequestURL)) {
            uriBuilder.appendPath(mRequestURL);
        }

        if (params != null) {
            for (final QueryParam queryParam : params) {
                uriBuilder.appendQueryParameter(queryParam.paramKey, queryParam.paramValue);
            }
        }

        //Finally add the api key if one is provided
        uriBuilder.appendQueryParameter(apiKeyString, apiKey);

        final URL finalUrl;
        HttpURLConnection urlConnection = null;
        try {
            finalUrl = new URL(uriBuilder.build().toString());
            urlConnection = (HttpURLConnection) finalUrl.openConnection();
            urlConnection.setRequestMethod(mRequestType.getValue());
            final InputStream inputStream = urlConnection.getInputStream();
            if (inputStream == null) {
                Log.e(TAG, "Connection to " + finalUrl.toString() + " returned null input stream");
                return null;
            }

            final InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            final BufferedReader in = new BufferedReader(inputStreamReader);
            final StringBuilder finalOutput = new StringBuilder();

            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                finalOutput.append(inputLine);
            }

            in.close();

            //Return the string response as is for the consumer to deal with it
            return finalOutput.toString();
        } catch (final IOException ioe) {
            mException = ioe;
            Log.e(TAG, ioe.getMessage());
            ioe.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return null;
    }

    /**
     * Calls the {@link PostExecuteCallback} with the response object received <p> {@inheritDoc}
     */
    @Override
    protected void onPostExecute(final String responseObj) {
        if (responseObj == null && mException == null) {
            return;
        }

        mPostExecuteCallback.onResponse(responseObj, mException);
    }
}