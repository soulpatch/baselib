package com.soulpatch.baselib.json;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Custom deserializer for removing nulls from a collection that is part of a JSON representation
 *
 * @author Akshay Viswanathan
 */
public class RemoveNullDeserializer<T> implements JsonDeserializer<List<T>> {
    private final Class mClazz;

    /**
     * Default constructor for
     *
     * @param clazz Should be the same class as the generic type T
     */
    public RemoveNullDeserializer(final Class clazz) {
        mClazz = clazz;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> deserialize(final JsonElement jsonElement, final Type type, final JsonDeserializationContext context) throws
            JsonParseException {

        final JsonArray jsonArray = jsonElement.getAsJsonArray();

        final List<T> list = new ArrayList<>();
        final Gson gson = new Gson();

        for (final JsonElement element : jsonArray) {
            // skipping the null here, if not null then parse json element and add in collection
            if (!(element instanceof JsonNull)) {
                //noinspection unchecked
                list.add((T) gson.fromJson(element, mClazz));
            }
        }

        return list;
    }
}