import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Testing purposes only
 */
public class JavaHttpsExample {
    public static void main(String[] args) throws Exception {
        String HospitalListURL = "https://data.gov.in/api/datastore/resource.json?resource_id=7d208ae4-5d65-47ec-8cb8-2a7a7ac89f8c&api-key=";
        String DispensaryListURL = "https://data.gov.in/api/datastore/resource.json?resource_id=0578a6c3-056e-4182-af7a-4307f1e0c2a7&api-key=";
        String BloodBankListURL = "https://data.gov.in/api/datastore/resource.json/?resource_id=e16c75b6-7ee6-4ade-8e1f-2cd3043ff4c9&api-key=";
        String apiKey = "276853f77e97da7488ce822d3655babe";
        URL myurl = new URL(BloodBankListURL + apiKey + "&filters[state]=\"Punjab\"");
        System.out.println(myurl);
        HttpsURLConnection con = (HttpsURLConnection) myurl.openConnection();
        InputStream ins = con.getInputStream();
        InputStreamReader isr = new InputStreamReader(ins);
        BufferedReader in = new BufferedReader(isr);

        writeToFile(in, "BloodBankList.json");
        // String inputLine;
//  
//     while ((inputLine = in.readLine()) != null) {
//       System.out.println(inputLine);
//     }

        in.close();
    }

    private static void writeToFile(final BufferedReader bufReader, final String fileName) {
        try {
            File file = new File(fileName);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            String inputLine;
            while ((inputLine = bufReader.readLine()) != null) {
                bw.write(inputLine);
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}