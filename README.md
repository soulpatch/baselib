# BaseLib #

Base library that provides some of the boilerplate code used in projects in general.
I developed this to reduce the time of development and copy paste errors moving forward as all the util methods could be in one place and adding this library will provide the application with all the methods.
Also since this will be tested in multiple scenarios as a result of use in multiple applications, it will be a refined product as opposed to it being in a single application and used in a specific workflow.

Add the library to the subproject gradle like so : 

***compile 'com.soulpatch.baselib:baselib:<Version>'***